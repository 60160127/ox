import java.util.*;

public class oxgame {

	public static void main(String[] args) {
		Scanner num = new Scanner(System.in);

		String xo[][] = { { "-", "-", "-" }, { "-", "-", "-" }, { "-", "-", "-" } };

		for (int i = 0; i < 9; i++) {
			if (i % 2 == 0) {
				System.out.print("X (R,C) : ");
				int r = num.nextInt();
				int c = num.nextInt();
				xo[r - 1][c - 1] = "x";

				for (int x = 0; x < 3; x++) {
					for (int y = 0; y < 3; y++) {
						System.out.print(xo[x][y] + " ");
					}
					System.out.println();
				}
				if ((xo[0][0].equals("x") && xo[0][1].equals("x") && xo[0][2].equals("x"))
						|| (xo[1][0].equals("x") && xo[1][1].equals("x") && xo[1][2].equals("x"))
						|| (xo[2][0].equals("x") && xo[2][1].equals("x") && xo[2][2].equals("x"))
						|| (xo[0][0].equals("x") && xo[1][0].equals("x") && xo[2][0].equals("x"))
						|| (xo[0][1].equals("x") && xo[1][1].equals("x") && xo[2][1].equals("x"))
						|| (xo[0][2].equals("x") && xo[1][2].equals("x") && xo[2][2].equals("x"))
						|| (xo[0][0].equals("x") && xo[1][1].equals("x") && xo[2][2].equals("x"))
						|| (xo[0][2].equals("x") && xo[1][1].equals("x") && xo[2][0].equals("x"))) {
					System.out.println("X win");
					break;

				} else {
					if (i == 8) {
						System.out.println("Always");
					}
				}

			} else {
				if (i % 2 != 0) {
					System.out.print("O (R,C) : ");
					int r = num.nextInt();
					int c = num.nextInt();
					xo[r - 1][c - 1] = "o";
					for (int x = 0; x < 3; x++) {
						for (int y = 0; y < 3; y++) {
							System.out.print(xo[x][y] + " ");
						}
						System.out.println();
					}
					if ((xo[0][0].equals("o") && xo[0][1].equals("o") && xo[0][2].equals("o"))
							|| (xo[1][0].equals("o") && xo[1][1].equals("o") && xo[1][2].equals("o"))
							|| (xo[2][0].equals("o") && xo[2][1].equals("o") && xo[2][2].equals("o"))
							|| (xo[0][0].equals("o") && xo[1][0].equals("o") && xo[2][0].equals("o"))
							|| (xo[0][1].equals("o") && xo[1][1].equals("o") && xo[2][1].equals("o"))
							|| (xo[0][2].equals("o") && xo[1][2].equals("o") && xo[2][2].equals("o"))
							|| (xo[0][0].equals("o") && xo[1][1].equals("o") && xo[2][2].equals("o"))
							|| (xo[0][2].equals("o") && xo[1][1].equals("o") && xo[2][0].equals("o"))) {
						System.out.println("O win");
						break;

					}
				}
			}
		}
	}

}
