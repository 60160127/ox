import java.util.*;

public class xo {

	static char table[][] = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };
	static char turn = 'x';
	static int count = 0;

	static void showWelcome() {
		System.out.println(" Welcome to XO Game ");
	}

	static void showTable() {
		System.out.println(" 1 2 3");
		for (int i = 0; i < 3; i++) {
			System.out.print(i + 1);
			for (int j = 0; j < 3; j++) {
				System.out.print(table[i][j] + " ");
			}
			System.out.println();
		}

	}

	static void showTurn() {

		if (count % 2 == 0) {
			System.out.println("X turn");

		} else {
			System.out.println("O turn");
		}

	}

	static void showSpace() {
		System.out.println();
	}

	static boolean showInput(int trun) {
		Scanner num = new Scanner(System.in);
		System.out.print("Input row,col : ");
		int row = num.nextInt() - 1;
		int col = num.nextInt() - 1;

		if (!(row >= 0 && row <= 2 && col >= 0 && col <= 2)) {
			showSpace();
			return false;
		}
		if (table[row][col] == '-') {
			if (count % 2 == 0) {
				table[row][col] = 'x';
				count++;
			} else {
				table[row][col] = 'o';
				count++;
			}
			return true;

		} else {
			System.out.println("Please Input row,col Again");
			return false;
		}

	}

	static boolean checkWin() {

		if ((table[0][0] == ('x') && table[0][1] == ('x') && table[0][2] == ('x'))
				|| table[1][0] == ('x') && table[1][1] == ('x') & table[1][2] == ('x')
				|| table[2][0] == ('x') && table[2][1] == ('x') & table[2][2] == ('x')
				|| table[0][0] == ('x') && table[1][0] == ('x') & table[2][0] == ('x')
				|| table[0][1] == ('x') && table[1][1] == ('x') & table[2][1] == ('x')
				|| table[0][2] == ('x') && table[1][2] == ('x') & table[2][2] == ('x')
				|| table[0][0] == ('x') && table[1][1] == ('x') & table[2][2] == ('x')
				|| table[0][2] == ('x') && table[1][1] == ('x') & table[2][0] == ('x')) {
			System.out.println("Game X Win");
			return true;

		} else {
			if ((table[0][0] == ('o') && table[0][1] == ('o') && table[0][2] == ('o'))
					|| table[1][0] == ('o') && table[1][1] == ('o') & table[1][2] == ('o')
					|| table[2][0] == ('o') && table[2][1] == ('o') & table[2][2] == ('o')
					|| table[0][0] == ('o') && table[1][0] == ('o') & table[2][0] == ('o')
					|| table[0][1] == ('o') && table[1][1] == ('o') & table[2][1] == ('o')
					|| table[0][2] == ('o') && table[1][2] == ('o') & table[2][2] == ('o')
					|| table[0][0] == ('o') && table[1][1] == ('o') & table[2][2] == ('o')
					|| table[0][2] == ('o') && table[1][1] == ('o') & table[2][0] == ('o')) {
				System.out.println("Game O Win");
				return true;

			} else {
				if (count > 8) {
					System.out.println("Game Draw");
					return true;
				} else {
					return false;
				}
			}

		}

	}

	public static void main(String[] args) {
		showWelcome();
		for (;;) {
			showTable();
			if (checkWin() == true) {
				break;
			}
			showTurn();
			showInput(turn);
		}

	}

}